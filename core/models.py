from pyexpat import model
from sre_parse import CATEGORIES
from statistics import mode
from django.db import models

# Create your models here.

class Category(models.Model):
    category = models.CharField(max_length=10)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.category

class TinyUrl(models.Model):
    category = models.ForeignKey(Category, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=255, null=False)
    shortUrl = models.CharField(max_length=10, null=False, blank=True)
    longUrl = models.CharField(max_length=255, null=False)

    class Meta:
        verbose_name_plural = "TinyUrls"

    def __str__(self):
        return self.name


