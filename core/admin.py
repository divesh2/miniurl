import random, string
from django.contrib import admin
from django.contrib.auth.models import User, Group
from .models import *

class UrlAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        saved = False
        if len(obj.shortUrl) < 2:
            while not saved:
                try:
                    url = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 4))
                    obj.shortUrl = url
                    super().save_model(request, obj, form, change)
                    saved = True
                except:
                    pass
        else:
            super().save_model(request, obj, form, change)

admin.site.register(Category)
admin.site.register(TinyUrl, UrlAdmin)
admin.site.unregister([User, Group])