from django.urls import path

from .views import getUrl, home, getCat

urlpatterns = [
    path('', home, name="home"),
    path('cat/<str:cat>', getCat, name="cat"),
    path('<str:url>', getUrl)
]