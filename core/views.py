from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required
from core.models import Category, TinyUrl

# Create your views here.

@login_required
def home(request):
    categories = Category.objects.all()
    return render(request, "core/index.html", {"categories": categories})


@login_required
def getCat(request, cat):
    category = get_object_or_404(Category, category=cat)
    links = TinyUrl.objects.filter(category=category)
    return render(request, "core/cat.html", {"category": category, "links": links})


def getUrl(request, url):
    tinyUrl = get_object_or_404(TinyUrl, shortUrl=url)
    return redirect(tinyUrl.longUrl)